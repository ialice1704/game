
<?php


class Team{
   public $name;
   
   //вывод
   public $fight_players = [];
   public $win;
   public $beaten;
   
   public function is_live(){
      if($this->live_players > 0) return true;
      else return false;
   }
   
   //--------
   public function __construct($name){
    $this->name = $name;
   }
   
   public $arr_races = array('Human_Warrior',
                  'Human_Archer',
                  'Human_Rider',
                  'Big_Orc',
                  'Elf_Archer',
                  'Elf_Warrior',
                  'Fallen_ELf',
                  'Free_Orc',
                  'Gnome_Warior');
  
  
     public $team = [];
     public $live_players = 10;
   
     //функция, выполняющая роль конструктора
     public function MyConstruct(array $race){
       foreach($race as $player)
       {
           $file  = 'models/'.strtolower($player.'.php');
   
           if (file_exists($file))
           {
               
               include_once($file);
   
               $class = ucfirst($player);
               array_push($this->team, new $class);
           }
       }
     }
   
     public function __call($name, array $race){
       if ($name == 'Construct'){
   
         if ( !empty($race[0]) ){                 //если в функцию переданы расы то
             $this->MyConstruct($race[0]);
         }else{
               $tmp_team = [];
   
               for ($i = 0; $i <= 9; $i++){
                   array_push($tmp_team, $this->arr_races[rand(0, 8)]);
               }
   
               $this->MyConstruct($tmp_team);
           }
       }
     }
   
     public function hit($enemy){
         $ally = $this->team[rand(0,9)]; //соратник
         $victim = $enemy->team[rand(0,9)]; // противник(жертва)
     
         if ( ($ally->live) && ($victim->live) )
         {
           $ally->hit($victim);
           
           if(!$victim->live) {
              
             array_push($this->fight_players, get_class($ally)." [$this->name] убил ".get_class($victim)." [$enemy->name]"."<br>");
             $enemy->live_players --;
             
             if($enemy->live_players == 0)
             {
               
               $this->beaten =  $enemy->name.' проиграла'."<br>";
               $this->win = $this->name.' победила'."<br>";
             }
           }
   
         }
   
   
  }
}


