<?php 
require_once('elf.php');
require_once('archer.php');

class Elf_Archer extends Elf {
use Archer;
       function __construct(){
              $this->armor = 0; //броня
              $this->damage = 45; // урон
              $this->speed = 62; //скорость
              $this->arrows=40;
              $this->accuracy = 10; // меткость
       }
}